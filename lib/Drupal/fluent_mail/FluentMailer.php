<?php

/**
 * @file
 * Contains \Drupal\fluent_mail\FluentMailer.
 */

namespace Drupal\fluent_mail;

use Drupal\Core\Mail\MailInterface;

/**
 * Provides a mailer object with a fluent interface.
 */
class FluentMailer {

  /**
   * An array containing message data.
   *
   * @var array
   */
  protected $message;

  /**
   * A MailInterface object.
   *
   * @var \Drupal\Core\Mail\MailInterface
   */
  protected $system;

  /**
   * Constructor.
   *
   * @param string $module
   *   The name of the module instantiating the object.
   * @param string $key
   *   A unique key to help identify this mailer.
   * @param \Drupal\Core\Mail\MailInterface $system
   *   A MainInterface object.
   */
  public function __construct($module, $key, MailInterface $system) {
    // Set the mail system property.
    $this->system = $system;

    // Set the message data defaults.
    $this->message = array(
      'id' => $module . '_' . $key,
      'module' => $module,
      'key' => $key,
      'headers' => array(),
      'langcode' => NULL,
      'from' => '',
      'to' => '',
      'subject' => '',
      'body' => array(),
      'params' => array(),
      'send' => TRUE,
    );
  }

  /**
   * Set the message subject.
   *
   * @param string $subject
   *   The message subject.
   */
  public function setSubject($subject) {
    $this->message['subject'] = $subject;
    return $this;
  }

  /**
   * Set the message body.
   *
   * The message body is an array containing multiple items which is ultimately
   * imploded to form the actual message body. This method will overwrite the
   * entire body array with a single item. See FluentMailer::setBodyPart() for
   * a method which will append new items to any existing body elements.
   * 
   * @param string $body
   *   The message body.
   */
  public function setBody($body) {
    $this->message['body'] = array($body);
    return $this;
  }

  /**
   * Append a new item to the message body.
   *
   * @param string $body
   *   A part of the message body.
   */
  public function setBodyPart($body) {
    $this->message['body'][] = $body;
    return $this;
  }

  /**
   * Set the address of the message recipient(s).
   *
   * @param string $to
   *   A list of email addresses.
   */
  public function setTo($to) {
    $this->message['to'] = $to;
    return $this;
  }

  /**
   * Set the address of the message sender.
   *
   * @param string $from
   *   An email address.
   */
  public function setFrom($from) {
    $this->message['from'] = $from;
    $this->setHeader('From', $from);
    return $this;
  }

  /**
   * Set the message langcode.
   *
   * @param string $langcode
   *   A langcode.
   */
  public function setLangcode($langcode) {
    $this->message['langcode'] = $langcode;
    return $this;
  }

  /**
   * Set the message headers.
   *
   * Note that this replaces the entire array of headers. See setHeader()
   * for a method that will set an individual header without replacing
   * everything.
   *
   * @param array $headers
   *   An associative array of message headers.
   */
  public function setHeaders(array $headers) {
    $this->message['headers'] = $headers;
    return $this;
  }

  /**
   * Set an individual message header.
   *
   * @param string $key
   *   The type of header.
   * @param string $value
   *   The value of the header.
   */
  public function setHeader($key, $value) {
    $this->message['headers'][$key] = $value;
    return $this;
  }

  /**
   * Set the message parameters.
   *
   * Note that this replaces the entire array of params. See setParam()
   * for a method that will set an individual param without replacing
   * everything.
   *
   * @param array $params
   *   An array of additional parameters.
   */
  public function setParams(array $params) {
    $this->message['params'] = $params;
    return $this;
  }

  /**
   * Set an individual message parameter.
   *
   * @param string $key
   *   The name of the parameter.
   * @param string $value
   *   The value of the parameter.
   */
  public function setParam($key, $value) {
    $this->message['params'][$key] = $value;
    return $this;
  }

  /**
   * Set the message sending status.
   *
   * @param boolean $send
   *   TRUE if the message should be sent immediately.
   */
  public function setSend($send) {
    $this->message['send'] = $send;
    return $this;
  }

  /**
   * Get the entire message data array.
   *
   * @return array
   *   The entire set of message data.
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * Send the message.
   *
   * @return array
   *   The entire message array.
   */
  public function send() {
    // Note that invoking hook_mail is probably unnecessary when using fluent
    // mail, but it is still called here if needed.
    // Build the e-mail (get subject and body, allow additional headers) by
    // invoking hook_mail() on this module. We cannot use module_invoke() as
    // we need to have $message by reference in hook_mail().
    if (function_exists($function = $this->message['module'] . '_mail')) {
      $function($this->message['key'], $this->message, $this->message['params']);
    }

    // Invoke hook_mail_alter() to allow all modules to alter the resulting e-mail.
    drupal_alter('mail', $this->message);

    // Format the message body.
    $this->message = $this->system->format($this->message);

    // The original caller requested sending. Sending was canceled by one or
    // more hook_mail_alter() implementations. We set 'result' to NULL, because
    // FALSE indicates an error in sending.
    if (empty($this->message['send'])) {
      $this->message['result'] = NULL;
    }
    // Sending was originally requested and was not canceled.
    else {
      $this->message['result'] = $this->system->mail($this->message);

      // Log errors.
      if (!$this->message['result']) {
        watchdog('mail', 'Error sending e-mail (from %from to %to).', array('%from' => $this->message['from'], '%to' => $this->message['to']), WATCHDOG_ERROR);
        drupal_set_message(t('Unable to send e-mail. Contact the site administrator if the problem persists.'), 'error');
      }
    }

    return $this->message;
  }

}
