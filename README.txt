Fluent Mail
-----------

Fluent Mail provides an alternative to drupal_mail() with fewer convolutions and a more pleasant developer experience via a fluent interface.  If you are not a developer or another module does not require it, you will derive no benefit from this module.  It makes no changes on its own.  To make use of the module call fluent_mail() in your code instead of drupal_mail() when you wish to send mail.  How is this an improvement?  Let's look at an example of sending a simple test message with drupal_mail():

<code>

/**
 * Implements hook_mail().
 */
function mymodule_mail($key, &$message, $params) {
  $message['subject'] = t('Test message');
  $message['body'][] = t('This is a test message.');
}

/**
 * Send a test email.
 */
function mymodule_send_test_mail() {
  drupal_mail('mymodule', 'test_message', 'me@example.com', language_default(), array(), 'admin@example.com', TRUE);
}

</code>

And the same example using fluent_mail():

<code>

/**
 * Send a test email.
 */
function mymodule_send_test_mail() {
  fluent_mail('mymodule', 'test_message')
    ->setTo('me@example.com')
    ->setFrom('admin@example.com')
    ->setSubject(t('Test message'))
    ->setBody(t('This is a test message.'))
    ->send();
}

</code>

To summarize, Fluent Mail is essentially just an object that wraps the equivalent of the $message array from drupal_mail(), provides a fluent interface for modifying the values of that array, and instantiates the object from a convenience function that sets some sensible defaults.  It still calls hook_mail_alter() and even hook_mail(), though implementing hook_mail() should be unecessary, and indeed making it unnecessary is one of the goals of this module.  It should also interact well with the rest of Drupal's mail system.  Maintaining compatibility with the existing base of mail related modules is another goal.

In addition to setting to, from, subject, and body a variety of other methods for setting headers, language, parameters, etc are also available.  See the FluentMailer class for the complete list.
